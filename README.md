# landing.cubicweb.org

https://naughty-neumann-a6cb27.netlify.com/

[![Netlify Status](https://api.netlify.com/api/v1/badges/c226bfdc-92b2-4e22-ad77-67c833e2361d/deploy-status)](https://app.netlify.com/sites/naughty-neumann-a6cb27/deploys)

## Getting started

### 0. Prerequisites

- **Node.js**&nbsp; `≥ v8.0`
- **NPM**&nbsp; `≥ v5.2`

### Develop
Spin up the development environment by running `npm run development`
By default, the build will be served at `localhost:3003`
The build system takes care of transpiling modified sources and triggering client updates

### Test
Run your test suite\* with `npm test`
Build, test and serve a production build for inspection with `npm run preflight`.
In addition to hosting the build on your local machine, the `preflight` task will also provision a tunneled, public URL for previewing the build outside your local network.

\* Currently, only linting for JavaScript is wired to the test task. You are responsible for integrating additional testing mechanisms as required.

## Documentation
The documentation can be found at [platframe.com/docs](https://platframe.com/docs)